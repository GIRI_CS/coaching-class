'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
       queryInterface.addConstraint(
        'courses', ['instructor'],
        {
          type: 'FOREIGN KEY',
          name: 'FK_courses_users_instructor',
          references: {
            table: 'users',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        }
      ),
       queryInterface.addConstraint(
        'enrollments', ['courseid'],
        {
          type: 'FOREIGN KEY',
          name: 'FK_enrollments_course_courseid',
          references: {
            table: 'courses',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        }
      ),
       queryInterface.addConstraint(
        'enrollments', ['studentid'],
        {
          type: 'FOREIGN KEY',
          name: 'FK_enrollments_users_studentid',
          references: {
            table: 'users',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        }
      ),
       queryInterface.addConstraint(
        'attendance', ['courseenrollmentid'],
        {
          type: 'FOREIGN KEY',
          name: 'FK_attendance_enrollments_courseenrollmentid',
          references: {
            table: 'enrollments',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        }
      ),
      ]
    );
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeConstraint('courses', 'FK_courses_users_instructor'),
      queryInterface.removeConstraint('enrollments', 'FK_enrollments_users_studentid'),
      queryInterface.removeConstraint('enrollments', 'FK_enrollments_course_courseid'),
    ]);
  }
}
