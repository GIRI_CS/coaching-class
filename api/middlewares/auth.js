const { intersection } = require('lodash');
const Promise = require('bluebird');
const env = require('../../config/vars').env;
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

exports.teacher = () => (req, res, next) => {
  if (env === "development") {
    return next();
  }
  try {
    const role = req.session.user.role;
    if(role === 'teacher'){
      return next();
    }
    return next(new APIError({
      status: httpStatus.UNAUTHORIZED,
      message: 'You are not allowed to access this resource',
    }));
  } catch (err) {
    return next(new APIError({
      status: httpStatus.UNAUTHORIZED,
      message: 'You are not allowed to access this resource',
    }));
  }
};


exports.student = () => (req, res, next) => {
  if (env === "development") {
    return next();
  }
  try {
    const role = req.session.user.role;
    if (role === 'student') {
      return next();
    }
    return next(new APIError({
      status: httpStatus.UNAUTHORIZED,
      message: 'You are not allowed to access this resource',
    }));
  } catch (err) {
    return next(new APIError({
      status: httpStatus.UNAUTHORIZED,
      message: 'You are not allowed to access this resource',
    }));
  }
};


exports.sessionChecker = (req, res, next) => {
  if (req.session && req.session.user && req.cookies.user_sid) {
    return req.session.user;
  } else {
    next();
  }
};

exports.logInChecker = (req, res, next) => {
  if (req.cookies && req.cookies.sid && !req.session.user) {
    res.clearCookie('user_sid');
  }
  next();
};