const express = require('express');
const { authorize, logInChecker, sessionChecker } = require('../../middlewares/auth');
const controller = require('../../controllers/user.controller');

const router = express.Router();

router
    .route('/signup')
    // .get(sessionChecker, (req, res, next) => {
    //     res.sendFile(__dirname + '/public/signup.html');
    // })
    .post(controller.create);

router.
route('/login')
    // .get(sessionChecker, (req, res) => {
    //     res.sendFile(__dirname + '/public/login.html');
    // })
    .post(logInChecker, controller.login);


router.
    route('/logout')
    // .get(sessionChecker, (req, res) => {
    //     res.sendFile(__dirname + '/public/login.html');
    // })
    .post(controller.logout);


module.exports = router;
