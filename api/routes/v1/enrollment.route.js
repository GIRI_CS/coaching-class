const express = require('express');
const controller = require('../../controllers/enrollment.controller');
const { student } = require('../../middlewares/auth');
const router = express.Router();

router
    .route('/')
    /**
     * @api {post} api/v1/enrollment Create enrollment
     * @apiDescription Create enrollment
     * @apiVersion 1.0.0
     * @apiName Create
     * @apiPermission student
     *
     *
     * @apiSuccess {Object} enrollment
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
     */
    .post(student(), controller.create);

router
    .route('/')
    /**
    * @api {post} api/v1/enrollment Create enrollment
    * @apiDescription Create enrollment
    * @apiVersion 1.0.0
    * @apiName Create
    * @apiPermission student
    *
    *
    * @apiSuccess {Object} enrollment
    *
    * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
    * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
    */
    .get(student(), controller.list);

router
    .route('/:id')
    /**
    * @api {post} api/v1/enrollment Create enrollment
    * @apiDescription Create enrollment
    * @apiVersion 1.0.0
    * @apiName Create
    * @apiPermission student
    *
    *
    * @apiSuccess {Object} enrollment
    *
    * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
    * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
    */
    .get(student(), controller.read);

router
    .route('/:id')
    /**
    * @api {post} api/v1/enrollment Create enrollment
    * @apiDescription Create enrollment
    * @apiVersion 1.0.0
    * @apiName Create
    * @apiPermission student
    *
    *
    * @apiSuccess {Object} enrollment
    *
    * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
    * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
    */
    .put(student(), controller.update);

router
    .route('/:id')
    /**
    * @api {post} api/v1/enrollment Create enrollment
    * @apiDescription Create enrollment
    * @apiVersion 1.0.0
    * @apiName Create
    * @apiPermission student
    *
    *
    * @apiSuccess {Object} enrollment
    *
    * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
    * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
    */
    .delete(student(), controller.delete);

module.exports = router;
