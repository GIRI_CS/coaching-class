const httpStatus = require('http-status');
const moment = require('moment');
const sequelize = require('../models').sequelize;

const User = sequelize.import('../models/user.model');
const { logger } = require('../../config/logger');
const {pick} = require('lodash');

/**
 * Create user
 * @public
 */
exports.create = async (req, res, next) => {
    try {
        const user = await User.create({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            role: req.body.role,
            password: req.body.password
        });
        const resp = pick(user, ['id', 'name', 'email', 'role']);
        logger.info('Valid user, setting session variables: ', user.dataValues);
        req.session.user = pick(user.dataValues, ['id', 'name', 'role', 'email']);
        return res.status(httpStatus.CREATED).json({ code: httpStatus.CREATED, message: 'User created successfully', resp });
    } catch (error) {
        return next(error);
    }
};

/**
 * Create user
 * @public
 */
exports.login = async (req, res, next) => {
    try {
        const username = req.body.username;
        const password = req.body.password;
        const user = await User.findOne({ where: { username: username } });
        if (!user) {
            logger.info('User not found');
            return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'User not found', user });
        } else 
            {
            const validPassword = await user.validPassword(password);
            if (!validPassword) {
                return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Invalid password' });
            } else {
                logger.info('Valid user, setting session variables: ', user.dataValues);
                const resUser = pick(user.dataValues, ['id', 'name', 'role', 'email']);
                req.session.user = resUser;
                return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'User logged in', resUser });
            }
        }
        
    } catch (error) {
        return next(error);
    }
};

exports.logout = async(req, res, next) => {
    logger.info(req.get('cookie'))

    // Upon logout, we can destroy the session and unset req.session.
    req.session.destroy(err => {
        // We can also clear out the cookie here. But even if we don't, the
        // session is already destroyed at this point, so either way, the
        // user won't be able to authenticate with that same cookie again.
        res.clearCookie('sid')

        // res.redirect('/')
        logger.info('User logged out');
        return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'User logged out' });
    })
};