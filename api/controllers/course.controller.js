const httpStatus = require('http-status');
const moment = require('moment');
const sequelize = require('../models').sequelize;
const { logger } = require('../../config/logger');
const Course = sequelize.import('../models/course.model');
const User = sequelize.import('../models/user.model');
const {
    isEmpty,
    toString,
    map,
    pick,
} = require('lodash');

/**
 * Create Course
 * @public
 */
exports.create = async (req, res, next) => {
    try {
        logger.info('User :', req.session.user);
        if (isEmpty(req.body.name) || isEmpty(toString(req.body.instructor))) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'One or more params missing' });
        }
        const params = pick(req.body, ['name', 'createdBy', 'instructor']);
        const instructor = await User.findOne({
            where: { id: params.instructor }
        });
        if (isEmpty(instructor)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'Instructor does not exist in system' });
        }
        let createdBy = {};
        if (!isEmpty(toString(params.createdBy))) {
            createdBy = await User.findOne({ where: { id: params.createdBy } });
            if (isEmpty(createdBy)) {
                return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'Creator does not exist in system' });
            }
        } else {
            params.createdBy = instructor.dataValues;
        }
        let course = await Course.create(req.body);
        course.instructor = pick(instructor.dataValues, ['id', 'username', 'email']);
        course.createdBy = pick(createdBy, ['id', 'username', 'email']);
        return res.status(httpStatus.CREATED).json({ code: httpStatus.CREATED, message: 'Course created successfully', course });
    } catch (error) {
        return next(error);
    }
};

/**
 * Read Course
 * @public
 */
exports.read = async (req, res, next) => {
    try {
        let course = await Course.findOne({
            where: { id: req.params.id }
        });
        if (course) {
            const instructor = await User.findOne({ id: course.instructor });
            const createdBy = await User.findOne({ id: course.createdBy });
            course.instructor = pick(instructor.dataValues, ['id', 'username', 'email']);
            course.createdBy = pick(createdBy.dataValues, ['id', 'username', 'email']);
            return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Client fetched successfully', course });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Course not found' });
    } catch (error) {
        return next(error);
    }
};

/**
 * List Courses
 * @public
 */
exports.list = async (req, res, next) => {
    try {
        let courses = await Course.findAll();
        courses = map(courses, course => course.dataValues);
        const data = [];
        await Promise.map(courses, async (course) => {
            const instructor = await User.findOne({ id: course.instructor });
            const createdBy = await User.findOne({ id: course.createdBy });
            course.instructor = pick(instructor.dataValues, ['id', 'username', 'username', 'email']);
            course.createdBy = pick(createdBy.dataValues, ['id', 'username', 'username', 'email']);
            data.push(course);
        });
        return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Course(s) fetched successfully', data });
    } catch (error) {
        return next(error);
    }
};

/**
 * Update Course
 * @public
 */
exports.update = async (req, res, next) => {
    try {
        let course = await Course.update(
            req.body,
            {
                where: {
                    id: req.params.id,
                },
            });

        if (course > 0) {
            course = await Course.findOne({
                where: { id: req.params.id }
            });
            const instructor = await User.findOne({ id: course.instructor });
            const createdBy = await User.findOne({ id: course.createdBy });
            course.instructor = pick(instructor.dataValues, ['id', 'username', 'username', 'email']);
            course.createdBy = pick(createdBy.dataValues, ['id', 'username', 'username', 'email']);
            return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Course updated successfully', course });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Course not found' });
    } catch (error) {
        return next(error);
    }
};

/**
 * Delete Course
 * @public
 */
exports.delete = async (req, res, next) => {
    try {
        if (isEmpty(req.params.id)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'One or more params missing' });
        }
        let course = await Course.findOne({
            where: { id: req.params.id }
        });
        if (isEmpty(course)) {
            return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Course not found' });
        }
        course = await Course.destroy({
            where: {
                id: req.params.id
            }
        });

        if (course > 0) {
            return res.status(httpStatus.NO_CONTENT).json({ code: httpStatus.NO_CONTENT, message: 'Course deleted' });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Course not found' });
    } catch (error) {
        return next(error);
    }
};
