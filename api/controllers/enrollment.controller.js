const httpStatus = require('http-status');
const moment = require('moment');
const sequelize = require('../models').sequelize;

const Enrollment = sequelize.import('../models/enrollment.model');
const User = sequelize.import('../models/user.model');
const Course = sequelize.import('../models/course.model');
const {
    isEmpty,
    toString,
    map,
    pick,
} = require('lodash');

/**
 * Create Course
 * @public
 */
exports.create = async (req, res, next) => {
    try {
        if (isEmpty(toString(req.body.studentid)) || isEmpty(toString(req.body.courseid))) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'One or more params missing' });
        }
        const params = pick(req.body, ['studentid', 'courseid']);
        const student = await User.findOne({
            where: { id: params.studentid }
        });
        const course = await Course.findOne({
            where: { id: params.courseid }
        });
        if (isEmpty(student)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'Student does not exist in system' });
        }
        if (isEmpty(course)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'Course does not exist in system' });
        }
        let enrollment = await Enrollment.create(params);
        enrollment = pick(enrollment.dataValues, ['id', 'enrolledOn']);
        enrollment.student = pick(student, ['id', 'username', 'email']);
        enrollment.course = pick(course, ['id', 'name', 'instructor']);
        return res.status(httpStatus.CREATED).json({ code: httpStatus.CREATED, message: 'Enrolled successfully', enrollment });
    } catch (error) {
        return next(error);
    }
};

/**
 * Read Course
 * @public
 */
exports.read = async (req, res, next) => {
    try {
        let enrollment = await Enrollment.findOne({
            where: { id: req.params.id }
        });
        if (enrollment) {
            enrollment = pick(enrollment.dataValues, ['id', 'enrolledon']);
            const student = await User.findOne({ id: enrollment.studentid });
            const course = await Course.findOne({ id: enrollment.courseid });
            enrollment.student = pick(student.dataValues, ['id', 'username', 'email']);
            enrollment.course = pick(course.dataValues, ['id', 'name', 'instructor']);
            return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Enrollment fetched successfully', enrollment });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Enrollment not found' });
    } catch (error) {
        return next(error);
    }
};

/**
 * List Courses
 * @public
 */
exports.list = async (req, res, next) => {
    try {
        let enrollments = await Enrollment.findAll();
        enrollments = map(enrollments, enrollment => enrollment.dataValues);
        const data = [];
        await Promise.map(enrollments, async (enrollment) => {
            const student = await User.findOne({ id: enrollment.studentid });
            const course = await Course.findOne({ id: enrollment.courseid });
            enrollment = pick(enrollment, ['id', 'enrolledon']);
            enrollment.student = pick(student.dataValues, ['id', 'username', 'email']);
            enrollment.course = pick(course.dataValues, ['id', 'name', 'instructor']);
            data.push(enrollment);
        });
        return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Enrollment(s) fetched successfully', data });
    } catch (error) {
        return next(error);
    }
};

/**
 * Update Course
 * @public
 */
exports.update = async (req, res, next) => {
    try {
        let params = {};
        if (!isEmpty(toString(req.body.courseid))){
            params.courseid = req.body.courseid;
        }
        if (!isEmpty(toString(req.body.studentid))) {
            params.studentid = req.body.studentid;
        }
        if(isEmpty(params)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'Params missing' });
        }
        const student = await User.findOne({ where: { id: params.studentid } });
        const course = await Course.findOne({ where: { id: params.courseid } });
        if(isEmpty(student) || isEmpty(course)){
            return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Course or student does not exist' });
        }
        let enrollment = await Enrollment.update(
            req.body,
            {
                where: {
                    id: req.params.id,
                },
            });

        if (enrollment > 0) {
            enrollment = await Enrollment.findOne({
                where: { id: req.params.id }
            });
            enrollment = enrollment.dataValues;
            
            enrollment.student = pick(student.dataValues, ['id', 'username', 'email']);
            enrollment.course = pick(course.dataValues, ['id', 'name', 'instructor']);
            return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Course updated successfully', enrollment });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Enrollment not found' });
    } catch (error) {
        return next(error);
    }
};

/**
 * Delete Course
 * @public
 */
exports.delete = async (req, res, next) => {
    try {
        if (isEmpty(req.params.id)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'One or more params missing' });
        }
        let enrollment = await Enrollment.findOne({
            where: { id: req.params.id }
        });
        if (isEmpty(enrollment)) {
            return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Enrollment not found' });
        }
        enrollment = await Enrollment.destroy({
            where: {
                id: req.params.id
            }
        });

        if (enrollment > 0) {
            return res.status(httpStatus.NO_CONTENT).json({ code: httpStatus.NO_CONTENT, message: 'Enrollment deleted' });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Enrollment not found' });
    } catch (error) {
        return next(error);
    }
};
